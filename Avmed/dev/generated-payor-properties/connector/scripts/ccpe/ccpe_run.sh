#!/bin/bash
set -e

HE_DIR=$1
mkdir -p $HE_DIR/ccpe/scripts
mkdir -p $HE_DIR/ccpe/logs
rm -rf $HE_DIR/ccpe/scripts/*
cp ccpe/ccpe-* $HE_DIR/ccpe/scripts
cp ccpe/ccpe_invocation.sh $HE_DIR/ccpe/scripts
cp ccpe/application.properties $HE_DIR/ccpe/scripts
